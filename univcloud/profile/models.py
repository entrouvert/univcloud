# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django.contrib.auth.models import User
from django.db import models

class UserProfile(models.Model):
    '''User Profile Model

    Stores the portal layout configuration, as json, in the layout field
    '''
    user = models.OneToOneField(User)
    layout = models.CharField(max_length=10000)

    def get_layout(self):
        return json.loads(self.layout)

    def set_layout(self, layout):
        self.layout = json.dumps(layout)
        self.save()
