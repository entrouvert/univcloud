# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'univcloud.views.homepage', name='homepage'),
    url(r'^add/(?P<appid>[a-z-]+)$', 'univcloud.views.add'),
    url(r'^remove/(?P<app_id>[a-z0-9-]+)$', 'univcloud.views.remove'),
    url(r'^color/(?P<app_id>[a-z0-9-]+)$', 'univcloud.views.color'),
    url(r'dragstop', 'univcloud.views.dragstop'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/logout/', 'django.contrib.auth.views.logout_then_login'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^authsaml2/', include('authentic2.authsaml2.urls')),
)
