# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Launcher App'''

import urlparse

from .base import BaseApp

class App(BaseApp):
    '''Launcher App, used to open a website full page'''
    template_name = 'apps/launcher.html'
    sizex = 2
    sizey = 1
    label = None
    url = None
    picto = None

    def __init__(self, app_id, label='label', url='#', picto=None, **kwargs):
        super(App, self).__init__(app_id, **kwargs)
        self.label = label
        self.url = url
        self.picto = picto

    def get_content_args(self):
        args = super(App, self).get_content_args()
        app_domain = None
        args.update({'url': self.url, 'label': self.label})
        if self.url:
            app_domain = urlparse.urlparse(self.url)[1]
            args['app_domain'] = app_domain
        if self.picto:
            args['app_picto'] = self.picto
        return args
