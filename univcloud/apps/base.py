# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Base class for Apps'''

from django.template.loader import render_to_string

class BaseApp(object):
    app_id = None
    sizex = 1
    sizey = 1
    template_name = None
    background_color = None
    scroll = False

    def __init__(self, app_id, row=1, col=1, background_color=None):
        self.app_id = app_id
        self.row = row
        self.col = col
        self.background_color = background_color

    def get_content(self):
        return render_to_string(self.template_name, self.get_content_args())
    content = property(get_content)

    def get_content_args(self):
        return {'app_id': self.app_id}
