# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import importlib

def create_app(app_id, app_name, kwargs):
    '''Create an application object of the requested type'''
    module = importlib.import_module('univcloud.apps.%s' % app_name)
    return module.App(app_id, **kwargs)
