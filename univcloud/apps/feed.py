# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Feed App'''

from .base import BaseApp

class App(BaseApp):
    '''Feed App, used to display a RSS feed'''
    app_id = 'feed-app'
    template_name = 'apps/feed.html'
    sizex = 2
    sizey = 2
    scroll = True

    def __init__(self, app_id, feed_url='#', **kwargs):
        super(App, self).__init__(app_id, **kwargs)
        self.feed_url = feed_url

    def get_content_args(self):
        args = super(App, self).get_content_args()
        args.update({'feed_url': self.feed_url})
        return args
