# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Weather App'''

from .base import BaseApp

class App(BaseApp):
    '''Weather App, used to display the current weather'''
    app_id = 'weather-app'
    template_name = 'apps/weather.html'
    location = None

    def __init__(self, app_id, location='615702', **kwargs):
        super(App, self).__init__(app_id, **kwargs)
        self.location = location

    def get_content_args(self):
        args = super(App, self).get_content_args()
        args.update({'location': self.location})
        return args
