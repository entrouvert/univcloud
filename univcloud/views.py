# -*- coding: utf-8 -*-
#
# UnivCloud Services Portal
# Copyright (C) 2013  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''Views for displaying and manipulating the homepage'''

import json
import random

from django.views.generic.base import TemplateView, RedirectView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from .profile.models import UserProfile

from . import apps

class Homepage(TemplateView):
    '''Homepage View, displays a serie of apps'''
    template_name = 'univcloud/homepage.html'

    def get_context_data(self, **kwargs):
        reset_layout = ('reset-layout' in self.request.GET)
        try:
            profile = self.request.user.get_profile()
        except UserProfile.DoesNotExist:
            profile = UserProfile()
            profile.user = self.request.user
            reset_layout = True

        if reset_layout:
            profile.set_layout({
                'app001': ('welcome', {'row': 1, 'col': 1,
                        'background_color': '#008ba5'}),
                'app002': ('add', {'row': 1, 'col': 3,
                        'background_color': '#583ab3'}),
                'app003': ('weather', {'row': 1, 'col': 4,
                        'background_color': '#2476eb'}),
                'app004': ('launcher', {'row': 1, 'col': 5,
                        'label': u'Demandes en ligne',
                        'picto': u'wcs',
                        'background_color': '#a500a6',
                        'url': 'http://univcloud.au-quotidien.com'}),
                #'app005': ('help', {'row': 2, 'col': 3}),
                'app006': ('feed', {'row': 2, 'col': 5,
                        'background_color': '#b91b46',
                        'feed_url': 'http://www.etudiant.gouv.fr/rid16/toutes-les-actualites.rss'}),
                'app007': ('feed', {'row': 3, 'col': 1,
                        'background_color': '#583ab3',
                        'feed_url': 'http://www.lemonde.fr/rss/une.xml'}),
                'app008': ('launcher', {'row': 2, 'col': 3,
                        'label': u'UNPIdF',
                        'picto': u'unpidf',
                        'background_color': '#d7562a',
                        'url': 'https://unpidf.univ-paris1.fr'}),
                'app009': ('launcher', {'row': 3, 'col': 3,
                        'label': u'RP',
                        'picto': u'reseaupro',
                        'background_color': '#008ba5',
                        'url': 'https://reseaupro.univ-paris1.fr/CAS.php'}),
                'app010': ('launcher', {'row': 4, 'col': 3,
                        'label': u'RP-2',
                        'picto': u'reseaupro-2',
                        'background_color': '#008ba5',
                        'url': 'https://reseaupro.univcloud.fr/univ-paris1/'}),
                 })
            profile.save()

        ctx = super(Homepage, self).get_context_data(**kwargs)
        ctx['apps'] = [apps.create_app(app_id, *x) for (app_id, x) in profile.get_layout().items()]
        return ctx

homepage = login_required(Homepage.as_view())


class Add(RedirectView):
    '''Add View, used to add a new application to the portal'''
    def get_redirect_url(self, appid):
        profile = self.request.user.get_profile()
        layout = profile.get_layout()
        layout['app%s' % random.randint(0, 10000)] = (appid, self.request.GET)
        profile.set_layout(layout)
        return reverse('homepage')

add = login_required(Add.as_view())


class Dragstop(RedirectView):
    '''Drag stop View, used to register the new position of an app'''
    def get_redirect_url(self):
        profile = self.request.user.get_profile()
        layout = profile.get_layout()
        for app in json.loads(self.request.GET['T']):
            if not 'id' in app:
                continue
            appid = app['id']
            if not appid in layout:
                continue
            layout[appid][1]['col'] = app['x']
            layout[appid][1]['row'] = app['y']
        profile.set_layout(layout)
        return reverse('homepage')

dragstop = login_required(Dragstop.as_view())


class Remove(RedirectView):
    '''Remove View, used to remove an app from the portal'''
    def get_redirect_url(self, app_id):
        profile = self.request.user.get_profile()
        layout = profile.get_layout()
        if app_id in layout:
            del layout[app_id]
            profile.set_layout(layout)
        return reverse('homepage')

remove = login_required(Remove.as_view())


class Color(RedirectView):
    '''Color View, used to set a new background color'''
    def get_redirect_url(self, app_id):
        profile = self.request.user.get_profile()
        layout = profile.get_layout()
        if app_id in layout:
            layout[app_id][1]['background_color'] = self.request.GET['bg']
            profile.set_layout(layout)
        return reverse('homepage')

color = login_required(Color.as_view())
