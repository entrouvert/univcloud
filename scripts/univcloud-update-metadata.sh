#!/bin/sh

MYDIR=$(dirname $(readlink -f $0))

MD=/var/tmp/univcloud-metadata
MDURL="https://services-federation.renater.fr/metadata/renater-metadata.xml"
MDCA=
MDCRT=
MDLOCAL=${MYDIR}/../metadata.local
VIRTUAL_ENV=${MYDIR}/../venv/
MANAGE=${MYDIR}/../manage.py

if [ -r /etc/univcloud.conf ]
then
  . /etc/univcloud.conf
fi


# lock to avoid concurrent updates
LOCK=/var/tmp/univcloud-update-metadata_in-progress.lock
if [ -r $LOCK ]
then
	PID=`cat $LOCK`
	ps waux | grep $PID | grep univcloud | grep -vq grep && exit
fi
unlock() {
	rm -f $LOCK
	exit
}
trap unlock INT TERM EXIT
echo $$ > $LOCK


# clean
rm -f $MD

#
# 1. Download throught HTTPS
#

FETCH=$MD.fetch.$$

if [ -r "$MDCA" ]
then
	echo "downloading IdPs metadata from $MDURL (ca=$MDCA)"
	wget --quiet --timeout=300 --ca-certificate=$MDCA -O $FETCH $MDURL
	RET=$?
else
	echo "downloading IdPs metadata from $MDURL (no-check-certificate)"
	wget --quiet --timeout=300 --no-check-certificate -O $FETCH $MDURL
	RET=$?
fi

if [ $RET -ne 0 ]
then
	rm -f $FETCH
	echo "error while downloading IdPs metadata (wget)"
	unset MD
	unset MDCRT
else
	mv -f $FETCH $MD
fi

#
# 2. Check metadatas
#

if [ -n "$MD" -a -r "$MDCRT" ]
then
	xmlsec1 --verify --pubkey-cert-pem $MDCRT $MD
	if [ $? -ne 0 ]
	then
		echo "error while checking signature of IdPs metadata (xmlsec1)"
		unset MD
	fi
elif [ -n "$MD" ]
then
	echo "WARNING: do not check signature of IdPs metadata"
fi

#
# 3. Insert metadata in univcloud database
#

# virtualenv activation 
export VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"
export PATH

if [ -n "$MD" ]
then
	python $MANAGE sync-metadata --traceback --source="federation" --idp --verbosity=1 $MD
fi

if [ -r "$MDLOCAL" ]
then
	python $MANAGE sync-metadata --traceback --source="local" --idp --verbosity=1 $MDLOCAL
fi

exit 0

