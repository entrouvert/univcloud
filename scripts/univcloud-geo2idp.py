#!/usr/bin/env python

'''
output a merge of idp + geo informations from discojuice
'''

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'univcloud.settings'

from authentic2.saml.common import get_idp_list_sorted
import sys
import json
import math
import urllib

def geo2idp(filename):
    idps = {}
    try:
        f = open(filename)
    except IOError, e:
        print >> sys.stderr, e
        return {}
    try:
        idp_list = json.load(f)
    except ValueError, e:
        f.close()
        print >> sys.stderr, 'reading %s: %s' % (filename, e)
        return {}
    f.close()
    if not idp_list:
        return {}
    if not isinstance(idp_list, list):
        print >> sys.stderr, '%s does not contain a list' % filename
        return {}
    for idp in idp_list:
        try:
            idps[idp['entityID']] = idp
        except Exception, e:
            print >> sys.stderr, 'bad geo information in %s (%s)' % (filename, idp)
    return idps

geo_idps = {}
for geofile in sys.argv[1:]:
    geo_idps.update(geo2idp(geofile))

n = 0
for idp in get_idp_list_sorted():
    n += 1
    entity_id = idp['entity_id']
    name = idp['name']
    geo = geo_idps.get(entity_id, {}).get('geo', { 'lat': 47.0 + 2.0*math.sin(n), 'lon':  2.5 + 3.0*math.cos(n) })
    href = '/authsaml2/sso?' + urllib.urlencode([('entity_id', entity_id)])
    li = u'<li><a href="%s" class="idplink" data-lat="%s" data-lon="%s" data-entityid="%s" data-filtertext="%s">%s</a></li>' % \
            (href, geo['lat'], geo['lon'], entity_id, name, name)
    print li.encode('utf-8')

