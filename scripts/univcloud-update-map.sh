#!/bin/sh

MYDIR=$(dirname $(readlink -f $0))

GEOURLS="https://static.discojuice.org/feeds/renater http://isos.univnautes.entrouvert.com/univnautes.geo"
GEODIR="/var/tmp/univcloud-geo/"
PYTHONPATH=$MYDIR/../
VIRTUAL_ENV=${PYTHONPATH}"venv/"
LOGINHTML=${PYTHONPATH}"univcloud/templates/univcloud/idplist.html"

if [ -r /etc/univcloud.conf ]
then
  . /etc/univcloud.conf
fi

mkdir -p $GEODIR

# virtualenv activation 
export VIRTUAL_ENV
PATH="$VIRTUAL_ENV/bin:$PATH"
export PATH
export PYTHONPATH

# lock to avoid concurrent updates
LOCK=/var/tmp/univcloud-update-map_in-progress.lock
if [ -r $LOCK ]
then
	PID=`cat $LOCK`
	ps waux | grep $PID | grep univcloud | grep -vq grep && exit
fi
unlock() {
	rm -f $LOCK
	exit
}
trap unlock INT TERM EXIT
echo $$ > $LOCK

n=1
for url in $GEOURLS
do
	GEOFILE=$GEODIR/$n
	wget -q --no-check-certificate -O $GEOFILE $url
	if [ $? -eq 0 ]
	then
		GEOFILES=$GEOFILES" "$GEOFILE
		n=$(($n+1))
	else
		echo "cannot download $url"
	fi
done

if [ -r $GEOLOCAL ]
then
	GEOFILES=$GEOFILES" "$GEOLOCAL
fi

# create indexhtml
python $MYDIR/univcloud-geo2idp.py $GEOFILES > $LOGINHTML

exit 0
