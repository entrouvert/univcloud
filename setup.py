#!/usr/bin/python
from setuptools import setup, find_packages
import os

setup(name='univcloud',
        version='0.1',
        license='AGPLv3',
        description='',
        url='https://dev.entrouvert.org/projects/univcloud/',
        download_url='http://repos.entrouvert.org/univcloud.git/',
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages(os.path.dirname(__file__) or '.'),
        install_requires=[
            'django < 1.6',
        ],
)
